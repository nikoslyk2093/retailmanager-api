﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class InvalidDateRangeException : BadRequestException
    {
        public InvalidDateRangeException()
            :base("The To date cannot be earlier than the From date")
        {
            
        }
    }
}
