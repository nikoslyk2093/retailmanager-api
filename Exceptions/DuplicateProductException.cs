﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class DuplicateProductException : BadRequestException
    {
        public DuplicateProductException(string code)
            : base($"Product with code {code} already exists")
        {
            
        }
    }
}
