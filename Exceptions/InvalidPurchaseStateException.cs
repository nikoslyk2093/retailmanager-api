﻿using RetailManager.API.Data.Entities;
using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class InvalidPurchaseStateException : BadRequestException
    {
        public InvalidPurchaseStateException(PurchaseState currentState)
            : base($"Order cannot be modified when state is {currentState}")
        {
            
        }
    }
}
