﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class RecordNotFoundException : NotFoundException
    {
        public RecordNotFoundException(string entity, object id)
            : base($"{entity} {id} not found in database")
        {
            
        }
    }
}
