﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class RequiredParametersMissingException : BadRequestException
    {
        public RequiredParametersMissingException(string message)
            : base(message) { }
    }
}
