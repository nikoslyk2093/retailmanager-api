﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class ParametersCannotBeNullException : BadRequestException
    {
        public ParametersCannotBeNullException()
            : base("You must provide at least one parameter")
        {
            
        }
    }
}
