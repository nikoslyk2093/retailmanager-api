﻿using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Exceptions
{
    public class CustomerContactRequiredException : BadRequestException
    {
        public CustomerContactRequiredException()
            : base("Customer must have at least one contact")
        {
            
        }
    }
}
