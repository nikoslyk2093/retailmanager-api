# Retail.API

A basic management system of a retail store
## Instructions

Change the server name of the SQL connection string in the appsettings.json

```json
{
  "ConnectionStrings": {
    "RetailDb": "Server={YOUR_SQL_INSTANCE};Database=RetailDb;Trusted_Connection=True;TrustServerCertificate=True"
  }
}
```

If you want the database to be automatically created and seeded at the first startup you can do this by setting **AutoDbCreation** to **true** in appsettings.json

```json
{
  "AutoDbCreation": false
}
```

You can also access Swagger in the following URL: https://localhost:{PORT}/swagger/index.html


## Notes

- The State of a purchase can have 3 valid values:
  - (1) - InProgress
  - (2) - Completed 
  - (3) - Canceled 
- If you want to add a product or modify an existing purchase you simply add a product in the products array and if it exists it will be modified and if not will be added at the purchase.

```json
{
  "id": "{existing_purchase_id}",
  "state": 1,
  "products": [
    {
      "id": "{existing_product_id}",
      "quantity": {new quantity}
    },
    {
      "id": "{new_product_id}",
      "quantity": {quantity}
    },
  ]
}
```
- A purchase cannot be modified if its state is (2)-Completed or (3)-Canceled
- At the search endpoints at least one query parameter must be used
   - Customers: [ name, surname, phone ]
   - Products: [ code (can be partial), name ] 
   - Purchases: [ phone, from, to, state ]
- In case of an error only managed exceptions return errors to the client. Everything else is logged and the client receives an HTTP Status 500 error.