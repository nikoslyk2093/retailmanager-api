﻿namespace RetailManager.API.WebContracts
{
    public class ApiListResponse<T>
    {
        public IEnumerable<T>? Results { get; set; }
    }
}
