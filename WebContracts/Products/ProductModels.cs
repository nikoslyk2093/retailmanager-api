﻿namespace RetailManager.API.WebContracts.Products
{
    /// <summary>
    /// ProductDto
    /// </summary>
    public class ProductDto
    {
        /// <summary>
        /// The unique code of the product
        /// </summary>
        public required string Code { get; set; }

        /// <summary>
        /// The name of the product
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// The description of the product
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// The category of the product
        /// </summary>
        public required string Category { get; set; }

        /// <summary>
        /// The price of the product
        /// </summary>
        public decimal Price { get; set; }
    }

    /// <summary>
    /// Product List Dto
    /// </summary>
    public class ProductListDto
    {
        /// <summary>
        /// The unique identifier of the Product
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The unique code of the product
        /// </summary>
        public required string Code { get; set; }

        /// <summary>
        /// The name of the product
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// The price of the product
        /// </summary>
        public decimal Price { get; set; }
    }

    public class ProductUpdateDto : ProductDto
    {
        /// <summary>
        /// The unique identifier of the Product
        /// </summary>
        public Guid Id { get; set; }
    }

    public class ProductDetailsDto : ProductDto
    {
        /// <summary>
        /// The unique identifier of the Product
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The creation date of the product
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// The modify date of the product
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
