﻿namespace RetailManager.API.WebContracts.Products
{
    public class SearchProductsByCriteriaRequest
    {
        public string? Code { get; set; }
        public string? Name { get; set; }
    }
}
