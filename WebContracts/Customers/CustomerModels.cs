﻿namespace RetailManager.API.WebContracts.Customers
{
    /// <summary>
    /// Customer Model
    /// </summary>
    public class CustomerDto
    {
        /// <summary>
        /// The name
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// The surname
        /// </summary>
        public required string Surname { get; set; }

        /// <summary>
        /// The email
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// The phone
        /// </summary>
        public string? Phone { get; set; }

        /// <summary>
        /// The address
        /// </summary>
        public string? Address { get; set; }

        /// <summary>
        /// The postal code
        /// </summary>
        public string? PostalCode { get; set; }

        /// <summary>
        /// The city
        /// </summary>
        public string? City { get; set; }

        /// <summary>
        /// The region
        /// </summary>
        public string? Region { get; set; }

        /// <summary>
        /// The notes
        /// </summary>
        public string? Notes { get; set; }
    }

    /// <summary>
    /// Customer List Model
    /// </summary>
    public class CustomerListDto
    {
        /// <summary>
        /// The unique identifier of the customer
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The name
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// The surname
        /// </summary>
        public required string Surname { get; set; }

        /// <summary>
        /// The email
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// The phone
        /// </summary>
        public string? Phone { get; set; }
    }

    public class CustomerUpdateDto : CustomerDto
    {
        /// <summary>
        /// The unique identifier of the customer
        /// </summary>
        public Guid Id { get; set; }
    }


    public class CustomerDetailsDto : CustomerDto
    {
        /// <summary>
        /// The unique identifier of the customer
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The creation date of the product
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// The modify date of the product
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
