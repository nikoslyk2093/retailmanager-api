﻿namespace RetailManager.API.WebContracts.Customers
{
    public class SearchCustomersByCriteriaRequest
    {
        public string? Name { get; set; }
        public string? Surname { get; set; }
        public string? Phone { get; set; }
    }
}
