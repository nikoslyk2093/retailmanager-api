﻿namespace RetailManager.API.WebContracts
{
    public class ErrorResponseDto
    {
        public string? Message { get; set; }
    }
}
