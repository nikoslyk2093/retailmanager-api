﻿using RetailManager.API.Data.Entities;
using RetailManager.API.WebContracts.Customers;

namespace RetailManager.API.WebContracts.Purchases
{
    /// <summary>
    /// Purchase Search Result Model
    /// </summary>
    public class PurchaseListDto
    {
        public Guid Id { get; set; }
        public string? Customer { get; set; }
        public PurchaseState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }

    /// <summary>
    /// Purchase Create Model
    /// </summary>
    public class PurchaseCreateDto
    {
        public PurchaseCreateDto()
        {
            Products = new();
        }

        public Guid CustomerId { get; set; }

        public List<PurchaseProductCreateDto> Products { get; set; }

        public class PurchaseProductCreateDto
        {
            public Guid ProductId { get; set; }
            public int Quantity { get; set; }
        }
    }

    /// <summary>
    /// Purchase Update Model
    /// </summary>
    public class PurchaseUpdateDto
    {
        public PurchaseUpdateDto()
        {
            Products = new();
        }

        public Guid Id { get; set; }
        public PurchaseState State { get; set; }
        public List<PurchaseProductUpdateDto> Products { get; set; }

        public class PurchaseProductUpdateDto
        {
            public Guid Id { get; set; }
            public int Quantity { get; set; }
        }
    }

    /// <summary>
    /// Purchase Details Model
    /// </summary>
    public class PurchaseDetailsDto
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public PurchaseState State { get; set; }

        public CustomerDetailsDto? Customer { get; set; }
        public List<PurchaseProductDetailsDto>? Products { get; set; }

        public class PurchaseProductDetailsDto
        {
            public Guid Id { get; set; }
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? Category { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }
        }
    }
}
