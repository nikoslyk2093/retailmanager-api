﻿using RetailManager.API.Data.Entities;

namespace RetailManager.API.WebContracts.Purchases
{
    public class SearchPurchasesByCriteriaRequest
    {
        public string? Phone { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public PurchaseState? State { get; set; }
    }
}
