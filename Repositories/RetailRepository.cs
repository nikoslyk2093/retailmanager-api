﻿using Microsoft.EntityFrameworkCore;
using RetailManager.API.Data;
using RetailManager.API.Data.Entities;
using System.Linq.Expressions;

namespace RetailManager.API.Repositories
{
    /// <summary>
    /// Contains common methods to retrieve and modify entities from the database
    /// </summary>
    /// <typeparam name="T">The entity</typeparam>
    public class RetailRepository<T> where T : Entity
    {
        private readonly AppDbContext _context;
        private readonly ILogger<RetailRepository<T>> _logger;

        /// <summary>
        /// Initializes a new instance of <see cref="RetailRepository{T}"/>
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        public RetailRepository(AppDbContext context, ILogger<RetailRepository<T>> logger)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves an <see cref="IQueryable{T}"/>
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetQuery()
        {
            return _context.Set<T>().AsQueryable<T>();
        }

        /// <summary>
        /// Retrieves an instance of <see cref="T"/> from the database using a custom expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns><see cref="T"/></returns>
        public async Task<T?> GetFirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            try
            {
                return await _context.Set<T>().FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(GetFirstOrDefaultAsync), ex.Message);

                throw;
            }
        }

        /// <summary>
        /// Retrieves a <see cref="List{T}"/> from the database using a custom expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression)
        {
            try
            {
                return await _context.Set<T>().Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(GetFirstOrDefaultAsync), ex.Message);

                throw;
            }
        }
        /// <summary>
        /// Returns a <see cref="bool"/> that indicates if a record exists in the database using a custom expression
        /// </summary>
        /// <param name="expression">The expression</param>
        /// <returns><see cref="bool"/></returns>
        public async Task<bool> ExistsAsync(Expression<Func<T, bool>> expression)
        {
            try
            {
                return await _context.Set<T>().AnyAsync(expression);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(ExistsAsync), ex.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new entity to the DbContext to be persisted after <see cref="SaveAsync"/> is called
        /// </summary>
        /// <param name="entity">The new entity</param>
        public async Task AddAsync(T entity)
        {
            try
            {
                await _context.Set<T>().AddAsync(entity);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(AddAsync), ex.Message);

                throw;
            }
        }

        /// <summary>
        /// Marks the <see cref="T"/> entity as modified in the DbContext so the changes will be persisted after <see cref="SaveAsync"/> is called
        /// </summary>
        /// <param name="entity">The updated entity</param>
        public void Update(T entity)
        {
            try
            {
                var entry = _context.Set<T>().Attach(entity);
                entry.State = EntityState.Modified;
                entry.Property(x => x.CreatedAt).IsModified = false;
                entry.Property(x => x.UpdatedAt).CurrentValue = DateTime.Now;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(Update), ex.Message);

                throw;
            }
        }

        /// <summary>
        /// Marks the <see cref="T"/> entity as deleted in the DbContext so it will be removed after <see cref="SaveAsync"/> is called
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Saves all changes made in this context to the database
        /// </summary>
        /// <returns>An <see cref="int"/> indicating how many records were affected</returns>
        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
