using Microsoft.EntityFrameworkCore;
using RetailManager.API.Data;
using RetailManager.API.Data.Entities;
using RetailManager.API.Middlewares.Exceptions;
using RetailManager.API.Repositories;
using RetailManager.API.Services;
using Serilog.Core;
using RetailManager.API.Helpers;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("RetailDb")));

Logger logger = LoggingBuilder.Build();
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddScoped<RetailRepository<Customer>>();
builder.Services.AddScoped<RetailRepository<Product>>();
builder.Services.AddScoped<RetailRepository<Purchase>>();
builder.Services.AddScoped<RetailRepository<PurchaseProduct>>();

builder.Services.AddScoped<CustomerService>();
builder.Services.AddScoped<ProductService>();
builder.Services.AddScoped<PurchaseService>();

var app = builder.Build();

if (builder.Configuration.GetValue<bool>("AutoDbCreation"))
{
    Seeder.CreateAndSeedDatabase(app.Services);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
