﻿using System.Linq.Expressions;

namespace RetailManager.API.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, 
            Expression<Func<T, bool>> filter, bool condition)
                => condition ? query.Where(filter) : query;
    }
}
