﻿using Microsoft.AspNetCore.Mvc;
using RetailManager.API.Services;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Customers;

namespace RetailManager.API.Controllers
{
    /// <summary>
    /// Customers Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly CustomerService _customerService;

        /// <summary>
        /// Initializes a new instance of <see cref="CustomersController"/>
        /// </summary>
        /// <param name="customerService">The customer service</param>
        public CustomersController(CustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Fetches a list of customers based on provided search criteria
        /// </summary>
        /// <param name="request">The search criteria</param>
        [HttpGet]
        [Route("Search")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiListResponse<CustomerListDto>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SearchCustomersAsync([FromQuery] SearchCustomersByCriteriaRequest request)
        {
            var response = await _customerService.SearchAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Fetches a customer based on its unique id
        /// </summary>
        /// <param name="id">The unique id of a customer</param>
        [HttpGet("{id}", Name = "GetCustomerById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerDetailsDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCustomerAsync([FromRoute] Guid id)
        {
            var response = await _customerService.GetByIdAsync(id);

            return Ok(response);
        }

        /// <summary>
        /// Creates a new customer
        /// </summary>
        /// <param name="request">The new customer information</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateCustomerAsync([FromBody] CustomerDto request)
        {
            var createdCustomer = await _customerService.CreateAsync(request);

            return CreatedAtRoute("GetCustomerById", new { id = createdCustomer.Id }, createdCustomer);
        }

        /// <summary>
        /// Updates a customer based on its unique id
        /// </summary>
        /// <param name="id">The customer unique id</param>
        /// <param name="request">The updated customer data</param>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCustomerAsync([FromRoute] Guid id, [FromBody] CustomerUpdateDto request)
        {
            if (id != request.Id)
                return BadRequest(new ErrorResponseDto
                {
                    Message = "Mismatched IDs in the request"
                });

            await _customerService.UpdateAsync(id, request);

            return Ok();
        }

        /// <summary>
        /// Deletes a customer based on its unique id
        /// </summary>
        /// <param name="id">The customer unique id</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteCustomerAsync([FromRoute] Guid id)
        {
            await _customerService.DeleteAsync(id);

            return Ok();
        }
    }
}
