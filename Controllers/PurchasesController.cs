﻿using Microsoft.AspNetCore.Mvc;
using RetailManager.API.Services;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Purchases;

namespace RetailManager.API.Controllers
{
    /// <summary>
    /// Purchases Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PurchasesController : ControllerBase
    {
        private readonly PurchaseService _purchaseService;

        /// <summary>
        /// Initializes a new instance of <see cref="PurchasesController"/>
        /// </summary>
        /// <param name="purchaseService"></param>
        public PurchasesController(PurchaseService purchaseService)
        {
            _purchaseService = purchaseService;
        }

        /// <summary>
        /// Fetches a list of purchases based on provided search criteria
        /// </summary>
        /// <param name="request">The search criteria</param>
        [HttpGet]
        [Route("Search")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiListResponse<PurchaseListDto>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SearchPurchasesAsync([FromQuery] SearchPurchasesByCriteriaRequest request)
        {
            var response = await _purchaseService.SearchAsync(request);

            return Ok(response);
        }


        /// <summary>
        /// Fetches a purchase based on its unique id
        /// </summary>
        /// <param name="id">The unique id of a purchase</param>
        [HttpGet("{id}", Name = "GetPurchaseById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PurchaseDetailsDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPurchaseAsync([FromRoute] Guid id)
        {
            var response = await _purchaseService.GetByIdAsync(id);

            return Ok(response);
        }

        /// <summary>
        /// Creates a new purchase for a customer
        /// </summary>
        /// <param name="request">The new purchase info</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreatePurchaseAsync([FromBody] PurchaseCreateDto request)
        {
            var createdPurchase = await _purchaseService.CreateAsync(request);

            return CreatedAtRoute("GetPurchaseById", new { id = createdPurchase!.Id }, createdPurchase);
        }

        /// <summary>
        /// Updates a purchase
        /// </summary>
        /// <param name="id">The unique identifier of the purchase</param>
        /// <param name="request">The updated purchase info</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePurchaseAsync([FromRoute] Guid id, [FromBody] PurchaseUpdateDto request)
        {
            if (request.Id != id)
                return BadRequest(new ErrorResponseDto
                {
                    Message = "Mismatched IDs in the request"
                });

            await _purchaseService.UpdateAsync(request);

            return Ok();
        }

        /// <summary>
        /// Removes a product from a purchase
        /// </summary>
        /// <param name="purchaseId">The purchase id</param>
        /// <param name="productId">The product id</param>
        /// <returns></returns>
        [HttpDelete("{purchaseId}/Products/{productId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeletePurchaseProductAsync([FromRoute] Guid purchaseId, [FromRoute] Guid productId)
        {
            await _purchaseService.RemovePurchaseProductAsync(purchaseId, productId);

            return Ok();
        }
    }
}
