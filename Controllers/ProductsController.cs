﻿using Microsoft.AspNetCore.Mvc;
using RetailManager.API.Services;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Products;

namespace RetailManager.API.Controllers
{
    /// <summary>
    /// Products Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductService _productService;

        /// <summary>
        /// Initializes a new instance of <see cref="ProductsController"/>
        /// </summary>
        /// <param name="productService">The product service</param>
        public ProductsController(ProductService productService)
        {
            _productService = productService;
        }

        /// <summary>
        /// Fetches a list of products based on provided search criteria
        /// </summary>
        /// <param name="request">The request parameters</param>
        [HttpGet]
        [Route("Search")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiListResponse<ProductListDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SearchProductsAsync([FromQuery] SearchProductsByCriteriaRequest request)
        {
            var response = await _productService.SearchAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Fetches a product based on its unique id
        /// </summary>
        /// <param name="id">The unique id of a product</param>
        [HttpGet("{id}", Name = "GetProductById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductDetailsDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetProductAsync([FromRoute] Guid id)
        {
            var response = await _productService.GetByIdAsync(id);

            return Ok(response);
        }

        /// <summary>
        /// Creates a new product
        /// </summary>
        /// <param name="request">The new product information</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateProductAsync([FromBody] ProductDto request)
        {
            var createdProduct = await _productService.CreateAsync(request);

            return CreatedAtRoute("GetProductById", new { id = createdProduct.Id }, createdProduct);
        }

        /// <summary>
        /// Updates a product
        /// </summary>
        /// <param name="id">The product unique id</param>
        /// <param name="request">The updated product data</param>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateProductAsync([FromRoute] Guid id, [FromBody] ProductUpdateDto request)
        {
            if (id != request.Id)
                return BadRequest(new ErrorResponseDto
                {
                    Message = "Mismatched IDs in the request"
                });

            await _productService.UpdateAsync(id, request);

            return Ok();
        }

        /// <summary>
        /// Deletes a product based on its unique id
        /// </summary>
        /// <param name="id">The product unique id</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteProductAsync([FromRoute] Guid id)
        {
            await _productService.DeleteAsync(id);

            return Ok();
        }
    }
}
