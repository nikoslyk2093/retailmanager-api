﻿using AutoMapper;
using RetailManager.API.Data.Entities;
using RetailManager.API.WebContracts.Products;

namespace RetailManager.API.Profiles
{
    public class ProductProfiles : Profile
    {
        public ProductProfiles()
        {
            CreateMap<Product, ProductListDto>().ReverseMap();
            CreateMap<Product, ProductDetailsDto>();
            CreateMap<ProductUpdateDto, Product>();
            CreateMap<ProductDto, Product>();
        }
    }
}
