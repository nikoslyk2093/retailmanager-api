﻿using AutoMapper;
using RetailManager.API.Data.Entities;
using RetailManager.API.WebContracts.Purchases;
using static RetailManager.API.WebContracts.Purchases.PurchaseDetailsDto;

namespace RetailManager.API.Profiles
{
    public class PurchaseProfiles : Profile
    {
        public PurchaseProfiles()
        {
            CreateMap<PurchaseCreateDto, Purchase>();
            CreateMap<PurchaseUpdateDto, Purchase>();
            CreateMap<Purchase, PurchaseListDto>();

            CreateMap<Purchase, PurchaseDetailsDto>()
                .ForMember(x => x.Customer, y => y.MapFrom(t => t.Customer))
                .ForMember(x => x.Products, c =>
                {
                    c.MapFrom(p => p.PurchaseProducts.Select(x => new PurchaseProductDetailsDto { Id = x.ProductId, Code = x.Product!.Code,
                        Name = x.Product.Name, Category = x.Product.Category, Price = x.Product.Price, Quantity = x.Quantity }));
                    c.PreCondition(x => x.PurchaseProducts != null);
                });


            CreateMap<PurchaseDetailsDto, Purchase>()
                .ForMember(x => x.Customer, y => y.Ignore())
                .ForMember(x => x.PurchaseProducts, c =>
                {
                    c.MapFrom(p => p.Products!.Select(pp => new PurchaseProduct { PurchaseId = p.Id, ProductId = pp.Id, 
                        Quantity = pp.Quantity }));
                    c.PreCondition(x => x.Products != null);
                });

            CreateMap<Purchase, PurchaseListDto>()
                .ForMember(x => x.Customer, c =>
                {
                    c.MapFrom(p => $"{p.Customer!.Name} {p.Customer!.Surname}");
                    c.PreCondition(x => x.Customer != null);
                });
        }
    }
}
