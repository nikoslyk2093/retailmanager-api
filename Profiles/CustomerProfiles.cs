﻿using AutoMapper;
using RetailManager.API.Data.Entities;
using RetailManager.API.WebContracts.Customers;

namespace RetailManager.API.Profiles
{
    public class CustomerProfiles : Profile
    {
        public CustomerProfiles()
        {
            CreateMap<CustomerDto, Customer>();
            CreateMap<CustomerUpdateDto, Customer>();
            CreateMap<Customer, CustomerDetailsDto>();
            CreateMap<Customer, CustomerListDto>();
        }
    }
}
