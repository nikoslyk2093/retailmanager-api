﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using System.Reflection;

namespace RetailManager.API.Helpers
{
    public class LoggingBuilder
    {
        public static Logger Build()
        {
            var appVersion = Assembly.GetExecutingAssembly()
            .GetCustomAttribute<AssemblyInformationalVersionAttribute>()!
            .InformationalVersion;

            var loggerConfiguration = new LoggerConfiguration();

            loggerConfiguration.MinimumLevel.Information();

            loggerConfiguration
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information);

            loggerConfiguration
                .Enrich.FromLogContext()
                .Enrich.WithProperty(nameof(appVersion), appVersion)
                .Enrich.WithProperty("Application Name", "Retail.API");

            loggerConfiguration.WriteTo.Console();

            return loggerConfiguration.CreateLogger();
        }
    }
}
