﻿using RetailManager.API.Data.Entities;

namespace RetailManager.API.Data
{
    public class Seeder
    {
        public static void CreateAndSeedDatabase(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            using var context = scope.ServiceProvider.GetRequiredService<AppDbContext>();

            if (context.Database.EnsureCreated())
            {
                var customer = new Customer { Name = "John", Surname = "Doe", Phone = "6999999999" };

                var products = new List<Product>
                {
                    new Product{ Code = "000001", Name = "iPhone 15 Pro Max", Category = "Mobile", Price = 1599.99m },
                    new Product{ Code = "000002", Name = "Xiaomi Redmi Note 11", Category = "Mobile", Price = 349.99m }
                };

                context.Customers.Add(customer);
                context.Products.AddRange(products);

                var purchase = new Purchase()
                {
                    CustomerId = customer.Id,
                    State = PurchaseState.InProgress
                };

                purchase.PurchaseProducts = new List<PurchaseProduct>()
                {
                    new PurchaseProduct { PurchaseId = purchase.Id, ProductId = products[0].Id, Quantity = 2 },
                    new PurchaseProduct { PurchaseId = purchase.Id, ProductId = products[1].Id, Quantity = 3 }
                };

                context.Purchases.Add(purchase);

                context.SaveChanges();
            }
        }
    }
}
