﻿namespace RetailManager.API.Data.Entities
{
    public enum PurchaseState
    {
        Unknown = 0,
        InProgress = 1,
        Completed = 2,
        Canceled = 3
    }
}
