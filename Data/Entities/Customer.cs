﻿namespace RetailManager.API.Data.Entities
{
    public class Customer : Entity
    {
        public Guid Id { get; set; }
        public required string Name { get; set; }
        public required string Surname { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? PostalCode { get; set; }
        public string? City { get; set; }
        public string? Region { get; set; } 
        public string? Notes { get; set; }

        public ICollection<Purchase>? Purchases { get; set; }
    }
}
