﻿namespace RetailManager.API.Data.Entities
{
    public class PurchaseProduct : Entity
    {
        public Guid PurchaseId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }

        public Purchase? Purchase { get; set; }
        public Product? Product { get; set; }
    }
}
