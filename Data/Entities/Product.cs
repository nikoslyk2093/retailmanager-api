﻿namespace RetailManager.API.Data.Entities
{
    public class Product : Entity
    {
        public Guid Id { get; set; }
        public required string Code { get; set; }
        public required string Name { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
        public decimal Price { get; set; }

        public ICollection<PurchaseProduct>? PurchaseProducts { get; set; }
    }
}
