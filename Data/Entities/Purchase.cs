﻿namespace RetailManager.API.Data.Entities
{
    public class Purchase : Entity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public PurchaseState State { get; set; }

        public Customer? Customer { get; set; }
        public ICollection<PurchaseProduct> PurchaseProducts { get; set; }
    }
}
