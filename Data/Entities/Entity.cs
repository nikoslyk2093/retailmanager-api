﻿namespace RetailManager.API.Data.Entities
{
    public class Entity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
