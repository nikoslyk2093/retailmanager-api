﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RetailManager.API.Data.Entities;
using System.Reflection.Emit;

namespace RetailManager.API.Data.Configurations
{
    public class PurchaseProductConfiguration : IEntityTypeConfiguration<PurchaseProduct>
    {
        public void Configure(EntityTypeBuilder<PurchaseProduct> builder)
        {
            builder.ToTable("PurchaseProducts");
            builder.HasKey(x => new { x.PurchaseId, x.ProductId });

            builder.Property(x => x.Quantity).IsRequired(true);
            builder.Property(x => x.CreatedAt).HasDefaultValueSql("GETDATE()").IsRequired(true);
            builder.Property(x => x.UpdatedAt).IsRequired(false);

            builder
                .HasOne(pp => pp.Purchase)
                .WithMany(p => p.PurchaseProducts)
                .HasForeignKey(pp => pp.PurchaseId);

            builder
                .HasOne(pp => pp.Product)
                .WithMany(pr => pr.PurchaseProducts)
                .HasForeignKey(pp => pp.ProductId);
        }
    }
}
