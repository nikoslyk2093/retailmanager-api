﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RetailManager.API.Data.Entities;

namespace RetailManager.API.Data.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).HasMaxLength(100).IsRequired(true);
            builder.Property(x => x.Surname).HasMaxLength(100).IsRequired(true);
            builder.Property(x => x.Email).HasMaxLength(100).IsRequired(false);
            builder.Property(x => x.Phone).HasMaxLength(20).IsRequired(false);
            builder.Property(x => x.Address).HasMaxLength(100).IsRequired(false);
            builder.Property(x => x.PostalCode).HasMaxLength(10).IsRequired(false);
            builder.Property(x => x.City).HasMaxLength(50).IsRequired(false);
            builder.Property(x => x.Region).HasMaxLength(50).IsRequired(false);
            builder.Property(x => x.Notes).HasMaxLength(1000).IsRequired(false);
            builder.Property(x => x.CreatedAt).HasDefaultValueSql("GETDATE()").IsRequired(true);
            builder.Property(x => x.UpdatedAt).IsRequired(false);

            builder.HasMany(x => x.Purchases)
                .WithOne()
                .HasForeignKey(x => x.CustomerId);
        }
    }
}
