﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RetailManager.API.Data.Entities;

namespace RetailManager.API.Data.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Code).HasMaxLength(20).IsRequired(true);
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired(true);
            builder.Property(x => x.Description).HasMaxLength(1000).IsRequired(false);
            builder.Property(x => x.Category).HasMaxLength(20).IsRequired(true);
            builder.Property(x => x.Price).HasPrecision(18,2).HasDefaultValue(0).IsRequired(true);
            builder.Property(x => x.CreatedAt).HasDefaultValueSql("GETDATE()").IsRequired(true);
            builder.Property(x => x.UpdatedAt).IsRequired(false);
        }
    }
}
