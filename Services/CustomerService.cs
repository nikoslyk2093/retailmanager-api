﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RetailManager.API.Data.Entities;
using RetailManager.API.Exceptions;
using RetailManager.API.Extensions;
using RetailManager.API.Repositories;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Customers;

namespace RetailManager.API.Services
{
    public class CustomerService
    {
        private readonly RetailRepository<Customer> _customerRepository;
        private readonly ILogger<CustomerService> _logger;
        private readonly IMapper _mapper;

        public CustomerService(RetailRepository<Customer> customerRepository, ILogger<CustomerService> logger, IMapper mapper)
        {
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ApiListResponse<CustomerListDto>> SearchAsync(SearchCustomersByCriteriaRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Name) && string.IsNullOrWhiteSpace(request.Surname) && string.IsNullOrWhiteSpace(request.Phone))
                    throw new ParametersCannotBeNullException();

                IEnumerable<Customer> customers  = await _customerRepository.GetQuery()
                    .WhereIf(x => x.Name == request.Name, !string.IsNullOrWhiteSpace(request.Name))
                    .WhereIf(x => x.Surname == request.Surname, !string.IsNullOrWhiteSpace(request.Surname))
                    .WhereIf(x => x.Phone == request.Phone, !string.IsNullOrWhiteSpace(request.Phone))
                    .ToListAsync();

                if (!customers.Any())
                    return new ApiListResponse<CustomerListDto>() { Results = new List<CustomerListDto>()};

                IEnumerable<CustomerListDto> mappedCustomers = _mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerListDto>>(customers);

                return new ApiListResponse<CustomerListDto> { Results = mappedCustomers };
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(SearchAsync), ex.Message);

                throw;
            }
        }

        public async Task<CustomerDetailsDto> GetByIdAsync(Guid id)
        {
            try
            {
                Customer? customer = await _customerRepository.GetFirstOrDefaultAsync(x => x.Id == id);

                if (customer == null)
                    throw new RecordNotFoundException(nameof(Customer), id);

                CustomerDetailsDto mappedCustomer = _mapper.Map<Customer, CustomerDetailsDto>(customer);

                return mappedCustomer;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(GetByIdAsync), ex.Message);

                throw;
            }
        }

        public async Task<CustomerDetailsDto> CreateAsync(CustomerDto newCustomer)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(newCustomer.Phone) && string.IsNullOrWhiteSpace(newCustomer.Email))
                    throw new CustomerContactRequiredException();

                var customerEntity = _mapper.Map<CustomerDto, Customer>(newCustomer);

                await _customerRepository.AddAsync(customerEntity);
                await _customerRepository.SaveAsync();

                var customerResponse = _mapper.Map<Customer, CustomerDetailsDto>(customerEntity);

                return customerResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(CreateAsync), ex.Message);

                throw;
            }
        }

        public async Task UpdateAsync(Guid id, CustomerUpdateDto updatedCustomer)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(updatedCustomer.Phone) && string.IsNullOrWhiteSpace(updatedCustomer.Email))
                    throw new CustomerContactRequiredException();

                if (!await _customerRepository.ExistsAsync(x => x.Id == id))
                    throw new RecordNotFoundException(nameof(Customer), id);

                var mappedCustomer = _mapper.Map<CustomerUpdateDto, Customer>(updatedCustomer);

                _customerRepository.Update(mappedCustomer);
                await _customerRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(UpdateAsync), ex.Message);

                throw;
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            try
            {
                Customer? customer = await _customerRepository.GetFirstOrDefaultAsync(x => x.Id == id);

                if (customer == null)
                    throw new RecordNotFoundException(nameof(Customer), id);

                _customerRepository.Delete(customer);
                await _customerRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(DeleteAsync), ex.Message);

                throw;
            }
        }
    }
}
