﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RetailManager.API.Data.Entities;
using RetailManager.API.Exceptions;
using RetailManager.API.Extensions;
using RetailManager.API.Repositories;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Products;

namespace RetailManager.API.Services
{
    public class ProductService
    {
        private readonly RetailRepository<Product> _productRepository;
        private readonly ILogger<ProductService> _logger;
        private readonly IMapper _mapper;

        public ProductService(RetailRepository<Product> ProductRepository, ILogger<ProductService> logger, IMapper mapper)
        {
            _productRepository = ProductRepository ?? throw new ArgumentNullException(nameof(ProductRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ApiListResponse<ProductListDto>> SearchAsync(SearchProductsByCriteriaRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Code) && string.IsNullOrWhiteSpace(request.Name))
                    throw new ParametersCannotBeNullException();

                IEnumerable<Product> products = await _productRepository.GetQuery()
                    .WhereIf(x => x.Code!.StartsWith(request.Code!), !string.IsNullOrWhiteSpace(request.Code))
                    .WhereIf(x => x.Name!.ToLower()!.StartsWith(request.Name!), !string.IsNullOrWhiteSpace(request.Name))
                    .ToListAsync();

                if (!products.Any())
                    return new ApiListResponse<ProductListDto>() { Results = new List<ProductListDto>() };

                IEnumerable<ProductListDto> mappedProducts = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductListDto>>(products);

                return new ApiListResponse<ProductListDto> { Results = mappedProducts };
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(SearchAsync), ex.Message);

                throw;
            }
        }

        public async Task<ProductDetailsDto> GetByIdAsync(Guid id)
        {
            try
            {
                Product? Product = await _productRepository.GetFirstOrDefaultAsync(x => x.Id == id);

                if (Product == null)
                    throw new RecordNotFoundException(nameof(Product), id);

                ProductDetailsDto mappedProduct = _mapper.Map<Product, ProductDetailsDto>(Product);

                return mappedProduct;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(GetByIdAsync), ex.Message);

                throw;
            }
        }

        public async Task<ProductDetailsDto> CreateAsync(ProductDto newProduct)
        {
            try
            {
                bool productExists = await _productRepository.ExistsAsync(x => x.Code == newProduct.Code);

                if(productExists)
                    throw new DuplicateProductException(newProduct.Code);

                var ProductEntity = _mapper.Map<ProductDto, Product>(newProduct);

                await _productRepository.AddAsync(ProductEntity);
                await _productRepository.SaveAsync();

                var ProductResponse = _mapper.Map<Product, ProductDetailsDto>(ProductEntity);

                return ProductResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(CreateAsync), ex.Message);

                throw;
            }
        }

        public async Task UpdateAsync(Guid id, ProductUpdateDto updatedProduct)
        {
            try
            {
                if (!await _productRepository.ExistsAsync(x => x.Id == id))
                    throw new RecordNotFoundException(nameof(Product), id);

                var mappedProduct = _mapper.Map<ProductUpdateDto, Product>(updatedProduct);

                _productRepository.Update(mappedProduct);
                await _productRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(UpdateAsync), ex.Message);

                throw;
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            try
            {
                Product? Product = await _productRepository.GetFirstOrDefaultAsync(x => x.Id == id);

                if (Product == null)
                    throw new RecordNotFoundException(nameof(Product), id);

                _productRepository.Delete(Product);
                await _productRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(DeleteAsync), ex.Message);

                throw;
            }
        }
    }
}
