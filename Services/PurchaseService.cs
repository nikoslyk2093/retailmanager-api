﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RetailManager.API.Data.Entities;
using RetailManager.API.Exceptions;
using RetailManager.API.Repositories;
using RetailManager.API.WebContracts;
using RetailManager.API.WebContracts.Purchases;

namespace RetailManager.API.Services
{
    public class PurchaseService
    {
        private readonly RetailRepository<Purchase> _purchaseRepository;
        private readonly RetailRepository<Customer> _customerRepository;
        private readonly RetailRepository<Product> _productRepository;
        private readonly RetailRepository<PurchaseProduct> _purchaseProdRepository;

        private readonly ILogger<PurchaseService> _logger;
        private readonly IMapper _mapper;

        public PurchaseService(RetailRepository<Purchase> purchaseRepository, RetailRepository<Customer> customerRepository,
            RetailRepository<PurchaseProduct> purchaseProdRepository, ILogger<PurchaseService> logger, IMapper mapper, 
            RetailRepository<Product> productRepository)
        {
            _purchaseRepository = purchaseRepository ?? throw new ArgumentNullException(nameof(purchaseRepository));
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _purchaseProdRepository = purchaseProdRepository ?? throw new ArgumentNullException(nameof(purchaseProdRepository)); ;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
        }

        public async Task<ApiListResponse<PurchaseListDto>> SearchAsync(SearchPurchasesByCriteriaRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Phone) && request.State == null && request.From == null && request.To == null)
                    throw new RequiredParametersMissingException("Parameters cannot be null");

                if (request.From == null && request.To != null || request.From != null && request.To == null)
                    throw new RequiredParametersMissingException($"Both {nameof(request.From)} and {nameof(request.To)} parameters must be filled");

                if (request.From != null && request.To != null && request.To < request.From)
                    throw new InvalidDateRangeException();

                    IQueryable<Purchase> query = _purchaseRepository.GetQuery().Include(x => x.Customer);

                if (!string.IsNullOrWhiteSpace(request.Phone))
                    query = query.Where(x => x.Customer != null && x.Customer.Phone == request.Phone);

                if (request.State != null)
                    query = query.Where(x => x.State == request.State);

                if(request.From != null && request.To != null)
                {
                    query = query.Where(x => x.CreatedAt >= request.From && x.CreatedAt <= request.To);
                }

                var purchases = await query.ToListAsync();

                var mappedPurchases = _mapper.Map<List<Purchase>, List<PurchaseListDto>>(purchases);

                return new ApiListResponse<PurchaseListDto> { Results = mappedPurchases };
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(SearchAsync), ex.Message);

                throw;
            }
        }

        public async Task<PurchaseDetailsDto?> GetByIdAsync(Guid id)
        {
            try
            {
                var purchase = await _purchaseRepository.GetQuery()
                    .Include(x => x.Customer)
                    .Include(x => x.PurchaseProducts)!
                        .ThenInclude(x => x.Product)
                    .Where(x => x.Id == id).FirstOrDefaultAsync();

                if (purchase == null)
                    return null;

                var mappedPurchase = _mapper.Map<Purchase, PurchaseDetailsDto>(purchase);

                return mappedPurchase;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(GetByIdAsync), ex.Message);

                throw;
            }
        }

        public async Task<PurchaseDetailsDto?> CreateAsync(PurchaseCreateDto request)
        {
            try
            {
                if (!await _customerRepository.ExistsAsync(x => x.Id == request.CustomerId))
                    throw new RecordNotFoundException(nameof(Customer), request.CustomerId);

                IEnumerable<Product>? products = new List<Product>();

                if (request.Products.Any())
                {
                    IEnumerable<Guid> productIds = request.Products.Select(x => x.ProductId);
                    products = await _productRepository.GetListAsync(x => productIds.Contains(x.Id));

                    IEnumerable<Guid> productsNotFound = request.Products.Where(x => !products.Select(x => x.Id).Contains(x.ProductId)).Select(x => x.ProductId);

                    if(productsNotFound.Any())
                        throw new RecordNotFoundException(nameof(Product), string.Join(",", productsNotFound));
                }

                Purchase purchase = new()
                {
                    CustomerId = request.CustomerId,
                    State = PurchaseState.InProgress
                };

                if (request.Products.Any())
                {
                    purchase.PurchaseProducts = new List<PurchaseProduct>();

                    foreach (var product in request.Products)
                    {
                        var purchaseProduct = new PurchaseProduct
                        {
                            PurchaseId = purchase.Id,
                            ProductId = product.ProductId,
                            Quantity = product.Quantity
                        };


                        purchase.PurchaseProducts.Add(purchaseProduct);
                    }
                }

                await _purchaseRepository.AddAsync(purchase);
                await _purchaseRepository.SaveAsync();

                return await GetByIdAsync(purchase.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(CreateAsync), ex.Message);

                throw;
            }
        }

        public async Task UpdateAsync(PurchaseUpdateDto updatedPurchase)
        {
            try
            {
                var purchase = await _purchaseRepository.GetQuery()
                    .Include(x => x.PurchaseProducts)!
                        .ThenInclude(x => x.Product)
                    .Where(x => x.Id == updatedPurchase.Id)
                    .FirstOrDefaultAsync();

                if (purchase == null)
                    throw new RecordNotFoundException(nameof(Purchase), updatedPurchase.Id);

                if (purchase.State == PurchaseState.Completed || purchase.State == PurchaseState.Canceled)
                    throw new InvalidPurchaseStateException(purchase.State);

                purchase.State = updatedPurchase.State;

                if (updatedPurchase.Products.Any())
                {
                    foreach (var product in updatedPurchase.Products)
                    {
                        PurchaseProduct? productToUpdate = purchase.PurchaseProducts.FirstOrDefault(x => x.ProductId == product.Id);

                        if(productToUpdate != null)
                        {
                            productToUpdate.Quantity = product.Quantity;
                            productToUpdate.UpdatedAt = DateTime.Now;
                        }
                        else
                        {
                            var newPurchaseProduct = new PurchaseProduct()
                            {
                                PurchaseId = purchase.Id,
                                ProductId = product.Id,
                                Quantity = product.Quantity
                            };

                            purchase.PurchaseProducts.Add(newPurchaseProduct);
                        }
                    }
                }

                _purchaseRepository.Update(purchase);
                await _purchaseRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(UpdateAsync), ex.Message);

                throw;
            }
        }

        public async Task RemovePurchaseProductAsync(Guid purchaseId, Guid productId)
        {
            try
            {
                PurchaseProduct? purchaseProduct 
                    = await _purchaseProdRepository.GetFirstOrDefaultAsync(x => x.PurchaseId == purchaseId && x.ProductId == productId);

                if (purchaseProduct == null)
                    throw new RecordNotFoundException(nameof(PurchaseProduct), new { PurchaseId = purchaseId, ProductId = productId});

                _purchaseProdRepository.Delete(purchaseProduct);
                await _purchaseProdRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occured in {method} - Message: {message}",
                    nameof(RemovePurchaseProductAsync), ex.Message);

                throw;
            }
        }
    }
}
