﻿using System.Text.Json;
using RetailManager.API.Middlewares.Exceptions.BaseExceptions;

namespace RetailManager.API.Middlewares.Exceptions
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate requestDelegate;

        public ExceptionMiddleware(RequestDelegate _requestDelegate)
        {
            requestDelegate = _requestDelegate;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await requestDelegate(context);
            }
            catch (Exception ex)
            {
                await ThrowExceptionAsync(context, ex);
            }
        }

        public async Task ThrowExceptionAsync(HttpContext context, Exception ex)
        {
            ExceptionDetails exceptionDetails = new();
            HttpResponse response = context.Response;
            response.ContentType = "application/problem+json";

            if (ex is ApiManagedException managedException)
            {
                response.StatusCode = managedException.StatusCode;

                exceptionDetails.Message = managedException.ErrorMessage;

                await response.WriteAsync(JsonSerializer.Serialize(exceptionDetails));
            }
            else
            {
                exceptionDetails.Message = "Something went wrong!";

                await response.WriteAsync(JsonSerializer.Serialize(exceptionDetails));
            }
        }
    }
}
