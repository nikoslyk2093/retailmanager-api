﻿namespace RetailManager.API.Middlewares.Exceptions.BaseExceptions
{
    public class NotFoundException : ApiManagedException
    {
        public NotFoundException(string message)
            : base(404, message)
        {

        }
    }
}
