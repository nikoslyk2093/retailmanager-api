﻿namespace RetailManager.API.Middlewares.Exceptions.BaseExceptions
{
    public class ExceptionDetails
    {
        public string? Message { get; set; }
    }
}
