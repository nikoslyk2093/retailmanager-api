﻿namespace RetailManager.API.Middlewares.Exceptions.BaseExceptions
{
    public class BadRequestException : ApiManagedException
    {
        public BadRequestException(string message)
            : base(400, message)
        {

        }
    }
}
