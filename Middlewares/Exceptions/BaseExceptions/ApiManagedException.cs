﻿namespace RetailManager.API.Middlewares.Exceptions.BaseExceptions
{
    public class ApiManagedException : Exception
    {
        public ApiManagedException(int statusCode, string errorMessage)
            : base(errorMessage)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
        }

        public int StatusCode { get; set; }
        public string? ErrorCode { get; set; }
        public string? ErrorMessage { get; set; }
    }
}
